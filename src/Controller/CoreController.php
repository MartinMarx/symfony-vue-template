<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/", name="core_")
 *
 * Class CoreController
 * @package App\Controller
 */
class CoreController extends AbstractController
{
    /**
     * @Route("", name="homepage")
     */
    public function index(): Response
    {
        return $this->render('core/index.html.twig');
    }
}
