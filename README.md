# Install

Install Symfony dependencies first
```bash
symfony composer install
```

Then, install Vue dependencies
```bash
yarn install
```

# Run the server

To run the backend server:
```bash
symfony server --allow-http
```

Run the Vue app:
```bash
yarn dev-server
```

# What's next

The template comes already comes with the [Api Platform]('https://api-platform.com/') 
dependency for the Symfony app. You can use it to build a
full REST Api and communicate between your front app and
the server.  
