import Vue from 'vue'

import './styles/style.scss';

import Homepage from "./pages/Home/HomePage.vue";

new Vue({
    el: '#app',
    components: {
        Homepage
    }
})
