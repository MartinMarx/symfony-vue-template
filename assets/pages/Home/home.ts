import Vue from 'vue'

import '../../styles/style.scss';

import HomePage from "./HomePage.vue";

new Vue({
    el: '#app',
    components: {HomePage},
    render (h) {
        return h('HomePage', {attrs: {start: 100}})
    }
})
